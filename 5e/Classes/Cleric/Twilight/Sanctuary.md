# Base  
*This set will grant a cleric twilight sanctuary buttons with no use of table, any DM rights and any API. In two clicks: First click on a button on the macro bar, second click with crosshair on a token of effected character. For conditions, require extra to pick one to end.*  
## Character/Attributes & Abilities/Abilities  
**Name:** `TS` *or Any*  
**Description:**  
`&{template:traits} {{name=Twilight Sanctuary}} {{source=Daunt}} {{description=**+[[{[[d6[Roll]+@{Daunt|base_level}[LVL]]] -  [[0 + @{target|t0|hp_temp}[Current Temp HP]]], 0}kh1]]Temp HP for @{target|t0|token_name}!**`  
`*(Had before[[[[@{target|t0|hp_temp}+0]][Current Temp HP]]],Getting this turn$[[0]])*}}`  
***Check "Show in Macro Bar"!***  

---  
**Name:** `TSc` *or Any*  
**Description:**  
`&{template:traits} {{name=Twilight Sanctuary}} {{source=Daunt}} {{description=End an effect on **@{target|t0|token_name}** causing them to be **?{What|frightened|charmed}**!}}`  
***Check "Show in Macro Bar"!***  

---  
**Name:** `TSnpc` *or Any*  
**Description:**  
`&{template:traits} {{name=Twilight Sanctuary}} {{source=Daunt}} {{description=**+[[{[[d6[Roll]+@{Daunt|base_level}[LVL]]]  -  [[0 + @{target|t0|bar1}[Current Temp HP]]], 0}kh1]]Temp HP for @{target|t0|token_name}!**`  
`*(Had before[[[[@{target|t0|bar1}+0]][Current Temp HP]]],Getting this turn$[[0]])*`  
`*Use green bar for tracking}}`  
***Check "Show in Macro Bar"!***  

---
# Extra
*For PC Temp HP tracking we use corresponding field on a Sheet, but for NPC it's not always set up, so TSnpc uses token bars (green bar by default).  
In case green bars in your game are occupied here are variations for other bars:*  
## Character/Attributes & Abilities/Abilities  
**Name:** `TSnpc-blue` *or Any*  
**Description:**  
`&{template:traits} {{name=Twilight Sanctuary}} {{source=Daunt}} {{description=**+[[{[[d6[Roll]+@{Daunt|base_level}[LVL]]]  -  [[0 + @{target|t0|bar2}[Current Temp HP]]], 0}kh1]]Temp HP for @{target|t0|token_name}!**`  
`*(Had before[[[[@{target|t0|bar2}+0]][Current Temp HP]]],Getting this turn$[[0]])*`  
`*Use blue bar for tracking}}`  
***Check "Show in Macro Bar"!***  

---  
**Name:** `TSnpc-red` *or Any*  
**Description:**  
`&{template:traits} {{name=Twilight Sanctuary}} {{source=Daunt}} {{description=**+[[{[[d6[Roll]+@{Daunt|base_level}[LVL]]]  -  [[0 + @{target|t0|bar3}[Current Temp HP]]], 0}kh1]]Temp HP for @{target|t0|token_name}!**`  
`*(Had before[[[[@{target|t0|bar3}+0]][Current Temp HP]]],Getting this turn$[[0]])*`  
`*Use red bar for tracking}}`  
***Check "Show in Macro Bar"!***  
