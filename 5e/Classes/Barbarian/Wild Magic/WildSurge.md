# Base  
*This set will grant a barbarian wild surge macro with no use of table, any DM rights and any API. In one click Wild Surge description pop up and d8 is rolled. This description has button, upon clikcing it provides numbers and flavour text of rolled Wild Surge.*  
## Character/Character Sheet/CORE/Feature & Traits  
**Name:** `[Wild Surge](~@{character_name}|WS[[[[d8]]]])`  
**Source:** *Any*  
**Source Type:** *Any*  
**Description:**  
`The magical energy roiling inside you sometimes erupts from you.`  
`When you enter your rage, roll on the Wild Magic table to determine the magical effect produced.`  
`If the effect requires a saving throw, the DC equals [[8 + @{pb}+@{constitution_mod}]] (8 + your proficiency bonus + your Constitution modifier)`  
`**Magical Effect$[[0]]engaged**`  
  
---  
## Character/Attributes & Abilities/Abilities  
**Name:** `WS1`  
**Description:**  
`&{template:dmg} {{rname={1} Shadowy Tendrils}} {{range=30 ft. radius}} {{damage=1}} {{dmg1flag=1}} {{dmg1=[[d12]]}} {{dmg1type=Necrotic}} {{dmg2flag=1}} {{dmg2=[[d12]]}} {{dmg2type=Temp HP}} {{save=1}} {{saveattr=Constitution}} {{savedesc=}} {{savedc=[[8 + @{pb}+@{constitution_mod}]]}} {{desc=Shadowy tendrils lash around you.`  
`Each creature of your choice that you can see within 30 feet of you must succeed on a Constitution saving throw or take $[[0]] necrotic damage.`  
`You also gain $[[1]] temporary hit points.}} {{charname=@{character_name}}}`  
  
---  
**Name:** `WS2`  
**Description:**  
`&{template:dmg} {{rname={2} Teleport}} {{range=30 ft.}} {{desc=You teleport up to 30 feet to an unoccupied space you can see.`  
`Until your rage ends, you can use this effect again on each of your turns as a bonus action.}} {{hldmg=hldmg}} {{spelllevel=spelllevel}} ammo=ammo {{charname=@{character_name}}}`  
  
  
---  
**Name:** `WS3`  
**Description:**  
`&{template:dmg} {{rname={3} Explosive Spirit}} {{range=30 ft./5 ft. radius}} {{damage=1}} {{dmg1flag=1}} {{dmg1=[[d6]]}} {{dmg1type=Force}} {{save=1}} {{saveattr=Dexterity}} {{savedesc=}} {{savedc=[[8 + @{pb}+@{constitution_mod}]]}} {{desc=An intangible spirit, which looks like a pixie, appears within 5 feet of one creature of your choice that you can see within 30 feet of you.`  
`At the end of the current turn, the spirit explodes, and each creature within 5 feet of it must succeed on a Dexterity saving throw or take d6 force damage.`  
`Until your rage ends, you can use this effect again, summoning another spirit, on each of your turns as a bonus action.}} {{charname=@{character_name}}}`  
  
  
---  
**Name:** `WS4`  
**Description:**  
`&{template:dmg} {{rname={4} Magic Weapon}} {{desc=Magic infuses one weapon of your choice that you are holding.`  
`Until your rage ends, the weapon's damage type changes to force, and it gains the light and thrown properties, with a normal range of 20 feet and a long range of 60 feet.`  
`If the weapon leaves your hand, the weapon reappears in your hand at the end of the current turn.}} {{charname=@{character_name}}}`  
  
  
---  
**Name:** `WS5`  
**Description:**  
`&{template:dmg} {{rname={5} Magical Retribution}} {{damage=1}} {{dmg1flag=1}} {{dmg1=[[d6]]}} {{dmg1type=Force}} {{desc=Whenever a creature hits you with an attack roll before your rage ends, that creature takes d6 force damage, as magic lashes out in retribution.}} {{charname=@{character_name}}}`  
  
---  
**Name:** `WS6`  
**Description:**  
`&{template:dmg} {{rname={6} Protective Lights}} {{range=10 ft. radius}} {{damage=1}} {{dmg1flag=1}} {{dmg1=**+[[1]]**}} {{dmg1type=AC}}{{desc=Until your rage ends, you are surrounded by multi colored, protective lights.`  
`You gain a +1 bonus to AC, and while within 10 feet of you, your allies gain the same bonus.}} {{charname=@{character_name}}}`  
  
---  
**Name:** `WS7`  
**Description:**  
`&{template:dmg} {{rname={7} Flowers and Vines}} {{range=15 ft. radius}} {{desc=Flowers and vines temporarily grow around you.`  
`Until your rage ends, the ground within 15 feet of you is difficult terrain for your enemies.}} {{charname=@{character_name}}}`  
  
---  
**Name:** `WS8`  
**Description:**  
`&{template:dmg} {{rname={8} Bolt of Light}} {{range=30 ft.}} {{damage=1}} {{dmg1flag=1}} {{dmg1=[[d6]]}} {{dmg1type=Radiant}}  {{save=1}} {{saveattr=Constitution}} {{savedesc=or damage and blindness}} {{savedc=[[8 + @{pb}+@{constitution_mod}]]}} {{desc=A bolt of light shoots from your chest.`  
`Another creature of your choice that you can see within 30 feet of you must succeed on a Constitution saving throw or take $[[0]] radiant damage and be blinded until the start of your next turn.`  
`Until your rage ends, you can use this effect again on each of your turns as a bonus action.}} {{charname=@{character_name}}}`  
  
---  
# Extra  
*This will add "attacks" for repeatable effects, that just use macros from previous section*  
## Character/Attributes & Abilities/Attributes  
**Name:** `@{percent}`  
**Current:** `%{`  
**Max:** *Any*  
## Character/Character Sheet/CORE/Attacks & Spellcasting  
**Name:** `{2} Teleport`  
***Uncheck all boxes!***  
**Description:** `@{percent}@{character_name}|WS2}`  
  
---  
**Name:** `{3} Explosive Spirit`  
***Uncheck all boxes!***  
**Description:** `@{percent}@{character_name}|WS3}`  
  
---  
**Name:** `{5} Magical Retribution`  
***Uncheck all boxes!***  
**Description:** `@{percent}@{character_name}|WS5}`  
  
---  
**Name:** `{8} Bolt of Light`  
***Uncheck all boxes!***  
**Description:** `@{percent}@{character_name}|WS8}`  
  
---  
  
